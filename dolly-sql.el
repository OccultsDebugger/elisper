;;;;;вот эта штука важна для орг-бабел
(org-babel-do-load-languages
 'org-babel-load-languages
 '((sql . t)))

;;;;создать память, где Долли будет запомать названия таблиц
(defvar memory-sql () "память, где Долли запоминает названия таблиц. название источника памяти таблицы - memorize-table")
(setq memory-sql "memorize-table")

;;;организуем забор атомов в память
;;;комманда к базе данных показывает все таблицы
(defun memorize-table () "Долли забирает из нужно базы все таблицы"
       (interactive)
       (save-excursion
	 (let* ((dbuser "vakas-tools.ru")
		(dbhost "31.31.203.139")
		(password "hF7zX6jV1p")
		(database (read-string "database: "))
		(command (format "mysql -h%s -u%s -p%s %s -Bse \"show tables\"" dbhost dbuser password database))
		(pre-memory (shell-command-to-string command))
		(memorize (progn
			    (go memory-sql)
			    (insert pre-memory)
			    (beginning-of-buffer)
			    (kill-line)
			    (save-buffer memory-sql)
			    (switch-to-buffer (other-buffer))
			    (get-string-from-file memory-sql)))
		(remember-table (with-temp-buffer
				  (insert-file-contents memory-sql)
				  (split-string (buffer-string) "\n" t)))
		(forget... (progn
			     (shell-command-to-string "rm -rf \"memorize-table\"")
			     (kill-buffer  "memorize-table"))))
	   (setq i 0)
	   (while (< i (length remember-table))
	     ;;;;//основной цикл сдесь...
	     (let* ((rt-name (format "%s" (nth i remember-table)))
		    (rt-command-desc (format "\n DESC %s " rt-name))
		    (rt-command-select (format "\n SELECT * FROM %s LIMIT 1" rt-name)))
	       (insert (format "\n* %s     :%s:" rt-name rt-name))
	       (insert (format "\n#+name: %s" rt-command-desc))
	       (insert (format "\n#+header: :engine mysql"))
	       (insert (format "\n#+header: :dbhost %s" dbhost))
	       (insert (format "\n#+header: :dbuser %s" dbuser))
	       (insert (format "\n#+header: :dbpassword %s" password))
	       (insert (format "\n#+header: :database %s" database))
	       (insert (format "\n#+begin_src sql"))
	       (insert rt-command-desc)
	       (insert "\n#+end_src")
	       (insert "\n")
	       (insert (format "\n#+name: %s" rt-command-select))
	       (insert (format "\n#+header: :engine mysql"))
	       (insert (format "\n#+header: :dbhost %s" dbhost))
	       (insert (format "\n#+header: :dbuser %s" dbuser))
	       (insert (format "\n#+header: :dbpassword %s" password))
	       (insert (format "\n#+header: :database %s" database))
	       (insert (format "\n#+begin_src sql"))
	       (insert rt-command-select)
	       (insert "\n#+end_src")
	       (insert "\n"))
	     (setq i (+ i 1)))
	   ;;;;////конец основного цикла...
	   (message "memorize table..."))))

;;(memorize-table)

(fset 'org-hack
      (kmacro-lambda-form [?\C-c ?\C-c ? ] 0 "%d"))

(defun xorg-hack () "не требует выставления в начало буффера"
  (interactive)
  (while (re-search-forward "src sql\n" nil t)
    (org-hack))
  (prepare-org-sql))

(defun prepare-org-sql () "подготовлю для работы"
       (interactive)
       (beginning-of-buffer)
       (while (search-forward "#+header" nil t)
	 (progn
	   (beginning-of-line)
	   (kill-line))))

(defun helper-org-sql () ""
       (interactive)
        (while (re-search-forward "[#+A-z]+" nil t)
	 (replace-match "")))



#+result
