#include <stdio.h>
#include <mysql.h>


void connect_exit(void)
{
  printf (stderr, "%s\n", mysql_error(con));
  exit(1);
}

void database_close(void)
{
  printf (stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);
}
