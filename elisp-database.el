;;;;инициализация файла для базы данных
(setq file-memory "memory-api")

;;;;описательный момент
(defvar MEMORY-API nil "База данных для работы с апи. Будет сохранять все необходимые данные для формирования сложного CURL запроса")
;;;;инициализация базы данных из памяти
(setq MEMORY-API (with-temp-buffer
		   (insert-file-contents file-memory)
		   (setq MEMORY-API (read (current-buffer)))))

;;;;сфира делает список юзера
(defun list-user (email password phone)
  (list
   :email email
   :password password
   :phone phone
   ))

;;;;(list-user "huravltampl@gmail.com" "12345" "877123477474" "access-token" "refresh-token" COOKIE)

;;;добавить юзера в базу данных
(defun push-user (user) (push user MEMORY-API))

;;;;вставка данных юзера
;;;;состоит из сфир list-user который принимает параметры и формирует список
;;;;push-user который применяет команду push
(defun memorize-MEMORY-API ()
  "вставка данных юзера"
  (let ((email (read-string "Введи email: "))
	(password (read-string "Введи пароль: "))
	(phone (read-string "Введи телефон: ")))
    ;;;;если логин не пустая строка
    (when (not (string-empty-p email))
      (push-user (list-user email password phone))))
  (message "Только что вставила данные в DATABASE-API ...insert...ok"))
;;;;(memorize-MEMORY-API)
;;;MEMORY-API


;;;следующий этап это сохранение локальной памяти в файл
;;;;эта cфира создает временный буффер с помощью макроса with-temp-buffer записывает в этот временный буффер все содержимое
;;;;DATABASE-API из этого временного буффера - и осуществляется запись в файл. Короче DATABASE-API записывается в файл
(defun save-MEMORY-API (filename)
  "эта cфира создает временный буффер с помощью макроса with-temp-buffer записывает в этот временный буффер все содержимое
DATABASE-API из этого временного буффера - и осуществляется запись в файл. Короче DATABASE-API записывается в файл. Если возвращаемое значение nil - значит MEMORY-API пустой атом. Так нельзя - всю базу удалишь!"
  (when (not (eq MEMORY-API nil))
    (with-temp-buffer
      (print MEMORY-API (current-buffer))
      (write-file filename))
    (message "В базу данных DATABASE-API только что произвела запись...ok")))
;;;;(save-MEMORY-API file-memory)

;;;;MEMORY-API

;;;;теперь вопрос как извлекать данные из базы данных с записью разобрались
(defun remember-MEMORY-API (file-memory)
  (with-temp-buffer
    (insert-file-contents file-memory)
    (setq MEMORY-API (read (current-buffer)))))
;;;;(remember-MEMORY-API file-memory)

;;;;обнуление MEMORY-API
;;;;(setq MEMORY-API '())
;;;;а теперь подумаем как извлекать из этого списка нужное мне
;;;;мне нужно извлечь queen@gmail
;;;;значит я в цикле прохожусь по всем атомам мегасписка (каждый атом это в свою очередь список юзера)


(defun remember-user-data () "Прога для получения данных из памяти"
       (let ((login (read-string "Login... ")))
	     ;;;если счетчик не нуль обнуляем
	 (when (not (eq i 0)) (setq i 0))
		   ;;;;все время пока счетчик меньше количества атомов в памяти
	 (while (< i memory-counter)
	   ;;(insert (format "\n%s " (getf (nth i MEMORY-API) :email)))
	   ;;;;если найден атом памяти с искомым логином
	   (if (string= (getf (nth i MEMORY-API) :email) login)
	       ;;;;то предлагается из атома выбрать другие атомы на выбор
	       (let ((choice (read-string "что вспомнить?\n(p) - password\n(h) - phone "))
		     (phone (getf (nth i MEMORY-API) :phone))
		     (password (getf (nth i MEMORY-API) :password)))
		 (cond ((equal choice "p")
			(insert (format "\npassword - %s " password)))
		       ((equal choice "h")
			(insert (format "\nphone - %s " phone)))
		       ((t (message "Введенные данные не валидные."))))))
	   ;;;(setq  udata (getf (nth i MEMORY-API) :phone))
	   ;;;в этом месте нужно остановить сфиру - чтобы Долли не тратила лишние силы
	   ;;  (setq udata (getf (nth i MEMORY-API) :phone) login)
	   ;;(insert (format "искомый телефон - %d " udata)))
	   ;;(message "Я, к сожалению, не помню такого юзера..."))
	   ;;;инкрементируем счетчик
	   (setq i (+ i 1))))
       (message "Только что выполнила воспоминание из памяти об юзерах. ---get-user-data---"))
;;;(remember-user-data)
;;;cookie - InfoCookie=somethong%20form%20somewhere


(defun observer-first (info)
  (print (format "первый наблюдатель наблюдает за %s" info)))

(defun observer-second (info)
  (print (format "второй наблюдатель наблюдает за %s" info)))

(observer-first 'card-diamonds)
(observer-second 'card-hearts)

(cons 'jacke 'queen)

(setq list-observers '())

(defun subscribe-x (observer)
  (cond ((null observer) (print '(слушатель не должен быть пустым)))
	(t (setq list-observers (append list-observers observer)) (print (format "слушатель %s добавлен в список слушателей" observer)))))

(subscribe-x 'info)

list-observers

(defvar info-user '((1 "Jacke" "diamonds" 25)
		(2 "Queen" "hearts" 22)
		(3 "Dolly" "spades")))

info-user

(defun create-user (id first-name last-name age)
  (setq info-user (append info-user (list (list id first-name last-name age))))
  "User created successfully")

(defun update-user (id first-name last-name age)
  (setf (nthcdr (- id 1) *users*) (list id first-name last-name age))
  "User updated successfully")

(defun delete-user (id)
  (setq info-user (remove-if (lambda (user) (= (car user) id)) info-user))
  "User deleted successfully")

(create-user 1 'king 'clubs 33)
(update-user 2 "BlackQueen" "Pretty" 21)
(delete-user 1)

info-user


;;Чтобы расширить окно вывода в Emacs при использовании функции print, можно использовать переменную print-length. Она определяет максимальное количество элементов списка, которые будут выведены на экран. По умолчанию значение этой переменной равно 12.
;;Чтобы установить новое значение переменной print-length, можно использовать следующий код:
;;;(setq print-length 40)
;;Этот код установит максимальное количество элементов списка, выводимых при использовании функции print, равным 100.
;;Также можно использовать переменную print-level, которая определяет максимальную глубину вложенности списков, которые будут выведены на экран. По умолчанию значение этой переменной равно nil, то есть все элементы списка будут выведены.
;;Чтобы установить новое значение переменной print-level, можно использовать следующий код:
;;;(setq print-level 10)
;;Этот код установит максимальную глубину вложенности списков, выводимых при использовании функции print, равной 3.
;;;(setq eval-expression-print-format 22)

(defun average-sum (lst)
  (cond ((null lst) 0)
	(t
	 (+
	  (car lst)
	  (average-sum (cdr lst))))))

(defun average (lst)
  (/ (average-sum lst) (length lst)))

(setq xlst '(101 100 99 100 101 102 90 99 100 101 102 103 110) )
(length xlst)


(average-sum xlst)

(average xlst)

(defun del (lst num)
  (cond
   ((eq num 0) (cdr lst))
   ((null lst) lst)
   (t
    (cons
     (car lst)
     (del (cdr lst) (- num 1)))
    (print (car lst)))))

(del xlst 2)

(trace-function 'del)
