;;;;Долли собирает курл по памяти
;;;;сфира принимает свет - содержимое памяти и по ним строит курл
(defun dolly-make-curl (user-agent cookie user-id access-token) "Долли собирает курл по памяти"
       	 ;;;;если сюда отразились не пустые атомы (строки)
       (when (not (string-empty-p user-agent))
	 (and (not (string-empty-p cookie)))
	 (and (not (string-empty-p user-id)))
	 (let* ((end-point END-POINT)
		(user-agent user-agent)
		(cookie cookie)
		(user-id user-id)
		(access-token access-token)
		(curl-request (format "curl -L -s -H 'Authorization: Bearer %s' -H 'User-Agent: %s' -H 'Cookie: %s' %s?user_id=%s" access-token user-agent cookie end-point user-id))
       	        ;;;;выполнить curl
		(curl-response-json (shell-command-to-string curl-request))
		;;;;обработать джсон в случае если пустой выдет сообщение о конце парсинга джсон
		(curl-info-list (json-read-from-string curl-response-json)))
	   ;;;;возможно так будет на выходе нужный мне атом
	   curl-info-list)))
