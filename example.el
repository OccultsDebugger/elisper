;;;;;;;пример полноценного круда с базой в 50 строчек кода на лиспе!!!
;;;;;;;список людей с номерами
(setq file-memory-user "memory-user")

(defvar MEMORY-USER nil "память о людях")

(setq MEMORY-USER (with-temp-buffer
		    (insert-file-contents file-memory-user)
		    (setq MEMORY-USER (read (current-buffer)))))

(defun list-user (firstname lastname email phone description) "сфира пишет инфу о людях"
       (list
	:firstname firstname
	:lastname lastname
	:email email
	:phone phone
	:description description))

(defun t-memorize-user () "память об юзерах"
       (let ((firstname (read-string "what is your name? "))
	     (lastname (read-string "your sorname? "))
	     (email (read-string "email? "))
	     (phone (read-string "phone? "))
	     (description (read-string "description? ")))
	 (when (not (string-empty-p firstname))
	   (push (list-user firstname lastname email phone description) MEMORY-USER))))
;;;(t-memorize-user)

(defun memorize-user () "память постоянная об юзерах"
       (when (not (eq MEMORY-USER nil))
	 (with-temp-buffer
	   (print MEMORY-USER (current-buffer))
	   (write-file file-memory-user))))
;;;;(memorize-user)

(defun remember-user () "вспомнить конкретного юзера"
       (when (not (eq i 0)) (setq i 0))
       (let ((user-name (read-string "Кого вспомнить? "))
	     (memory-counter (length MEMORY-USER)))
	 (while (< i memory-counter)
	   (if (string= (getf (nth i MEMORY-USER) :firstname) user-name)
	       (let ((firstname (getf (nth i MEMORY-USER) :firstname))
		     (lastname (getf (nth i MEMORY-USER) :lastname))
		     (email (getf (nth i MEMORY-USER) :email))
		     (phone (getf (nth i MEMORY-USER) :phone))
		     (description (getf (nth i MEMORY-USER) :description)))
		 (get-user firstname lastname email phone description)
		 (setq i memory-counter))
	     (setq i (+ i 1))))))
;;;(remember-user)

(defun get-user (firstname lastname email phone description)
  (when (not (string-empty-p firstname))
    (let ((choice (read-string "(f) - firstname, \n(l) - lastname, \n(e) - email, \n(p) - phone, \n(d) - description, \n(x) - exit ")))
      (unless (equal choice "x")
	(cond ((equal choice "f")
	       (insert (format "\nfirstname - %s" firstname)))
	      ((equal choice "l")
	       (insert (format "\nlastname - %s" lastname)))
	      ((equal choice "e")
	       (insert (format "\nemail - %s" email)))
	      ((equal choice "p")
	       (insert (format "\nphone - %s" phone)))
	      ((equal choice "d")
	       (insert (format "\ndescription - %s" description)))
	      (t (message "data is not valid")))
	(get-user firstname lastname email phone description)))))
;;;(get-user "queen" "diamonds" "jacke@gmail.com" "80480480480" "good jacke")



(defun init-memory (memory-file) "инициализация памяти"
       (if (file-exists-p memory-file)
	   (if (string-empty-p (get-string-from-file memory-file))
	       (progn
		 (message "память пуста и будет инициированна чистым атомом!")
		 (setq memory-sql ()))
	     (progn
	       (message "память будет загруженна!")
	       (setq memory-sql
		     (with-temp-buffer
		       (insert-file-contents "memorize-table")
		       (setq memory-sql (read (current-buffer)))))))
	 (message "Файла нет")))

 ;;(with-current-buffer (find-file-noselect "memory.sql")
;;  (mapcar (lambda (x) (split-string x " " t))
;;          (split-string
;;           (buffer-substring-no-properties (point-min) (point-max)))))
           


 (setq default-frame-alist
            '(
              (tool-bar-lines . 0)
              (width . 10)
              (height . 60)
              (background-color . "honeydew")
              (left . 50)
              (top . 50)))
  (progn
    (setq initial-frame-alist '( (tool-bar-lines . 0)))
    (setq default-frame-alist '( (tool-bar-lines . 0))))

(defun win-resize-top-or-bot ()
  "Figure out if the current window is on top, bottom or in the
middle"
  (let* ((win-edges (window-edges))
	 (this-window-y-min (nth 1 win-edges))
	 (this-window-y-max (nth 3 win-edges))
	 (fr-height (frame-height)))
    (cond
     ((eq 0 this-window-y-min) "top")
     ((eq (- fr-height 1) this-window-y-max) "bot")
     (t "mid"))))

(defun win-resize-left-or-right ()
  "Figure out if the current window is to the left, right or in the
middle"
  (let* ((win-edges (window-edges))
	 (this-window-x-min (nth 0 win-edges))
	 (this-window-x-max (nth 2 win-edges))
	 (fr-width (frame-width)))
    (cond
     ((eq 0 this-window-x-min) "left")
     ((eq (+ fr-width 4) this-window-x-max) "right")
     (t "mid"))))

(defun win-resize-enlarge-horiz ()
  (interactive)
  (cond
   ((equal "top" (win-resize-top-or-bot)) (enlarge-window -1))
   ((equal "bot" (win-resize-top-or-bot)) (enlarge-window 1))
   ((equal "mid" (win-resize-top-or-bot)) (enlarge-window -1))
   (t (message "nil"))))

(defun win-resize-minimize-horiz ()
  (interactive)
  (cond
   ((equal "top" (win-resize-top-or-bot)) (enlarge-window 1))
   ((equal "bot" (win-resize-top-or-bot)) (enlarge-window -1))
   ((equal "mid" (win-resize-top-or-bot)) (enlarge-window 1))
   (t (message "nil"))))

(defun win-resize-enlarge-vert ()
  (interactive)
  (cond
   ((equal "left" (win-resize-left-or-right)) (enlarge-window-horizontally -1))
   ((equal "right" (win-resize-left-or-right)) (enlarge-window-horizontally 1))
   ((equal "mid" (win-resize-left-or-right)) (enlarge-window-horizontally -1))))

(defun win-resize-minimize-vert ()
  (interactive)
  (cond
   ((equal "left" (win-resize-left-or-right)) (enlarge-window-horizontally 1))
   ((equal "right" (win-resize-left-or-right)) (enlarge-window-horizontally -1))
   ((equal "mid" (win-resize-left-or-right)) (enlarge-window-horizontally 1))))


(let ((i 0))
  (dotimes (i 4)
    (win-resize-minimize-vert)))

;;;;вот эта сфира считает количество открытых окон
;;;(length (cl-delete-duplicates (mapcar #'window-buffer (window-list))))



(defun win-resize-dotimes-8 ()
  (let ((i 0)
	(num 8))
    (dotimes (i num)
      (win-resize-minimize-vert)
      (print '(окно сдвинула чуть чуть)))))

(defun dolly-debugger () "Долли работает с пхп дебаггом"
       (interactive)
       ;;;если количество окно 1
       (cond ((eq (length (cl-delete-duplicates (mapcar #'window-buffer (window-list)))) 1)
	      (delete-other-windows)
	      (split-window-right -50))
	     (t (delete-other-windows) (print '(каждая строчка кода приносит Силу!)))))
(global-set-key (kbd "s-l") 'dolly-debugger)
