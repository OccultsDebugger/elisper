;;;;;Controller for dolly-url
;;;;;отсюда ORM для базы курла
(load "~/elisper/dolly-make-curl.el")
;;;;отсюда ORM для базы юзера
(load "~/elisper/elisp-database.el")
(load "~/elisper/dolly-url.el")

(setq END-POINT "http://api.vk.com/method/status.get?user_id=")
;;;видно что вк требует user_id

;;;;в этом месте должна выполнится сама прога и ключевая сфира
;;;;в этом месте должне быть клиентский пользовательский цикл
;;;;забавная ситуация с рекурсией и инициализацией пустоты! не повторяй чревато последствиями!
(defun curldolly () "контроллер для проги"
       (interactive)
       (let ((performance (read-number "Выбери перформанс: \n1. - чтобы запомнить нового юзера.\n2. - чтобы вспомнить юзера.\n3. - чтобы запомнить элементы для curl.\n4. - чтобы вспомнить апи.\n10 - чтобы выйти ")))
	 (unless (equal performance 10)
	   (cond ((equal performance 1)
		  ;;;;вставка данных юзера
		  (memorize-MEMORY-API)
		  (if (yes-or-no-p "Отлично. Если записать, то я запомню этого юзера. Записать? ")
		      (save-MEMORY-API file-memory))
		  (message "Выполнила performance-1"))
		 ((equal performance 2)
		  ;;;;вспоминаю юзера из памяти
		  (remember-MEMORY-API file-memory)
		  ;;;;вспоминаю теперь детали юзера
		  (remember-user-data)
		  (message "Вспомнила юзера. ---performance-2"))
		 ((equal performance 3)
		  ;;;;запоминание новых элементов
		  (t-memorize-MEMORY-CURL)
		  (if (yes-or-no-p "Отлично. Пару повторений, и апи запонмю. Запомнить? ")
		      (memorize-MEMORY-CURL file-memory-url))
		  (message "Запомнила элементы апи. ---performance-3---"))
		 ((equal performance 4)
		  ;;;;;вспоминание всего апи
		  (remember-MEMORY-CURL file-memory-url)
		  ;;;;;вспоминание элементов апи - и выполнение их
		  (remember-element-curl)
		  (message "Выполнила Апи. Удачной охоты! ---performance-4---"))
		 ((t (message "Выбери, please, вариант из предложенных."))))
	   (curldolly))))
;;;(curldolly)
(global-set-key (kbd "C-c ;") 'curldolly)
