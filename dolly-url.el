(load "~/elisper/dolly-make-curl.el")
;;(load "~/elisper/elisp-database.el")

;;;;;;инициализация файла для памяти
(setq file-memory-url "memory-url")

;;;;;симулируем браузер файрефокс
(setq USER-AGENT "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/112.0")

;;;описание
(defvar MEMORY-CURL nil "Память для Долли. Вспоминая Долли будет формировать curl запрос который будет в точности таким же как если бы его формировал браузер, зарегестрированного юзера")

;;;;инициализация памяти 
(setq MEMORY-CURL (with-temp-buffer
		   (insert-file-contents file-memory)
		   (setq MEMORY-CURL (read (current-buffer)))))

;;;;сфира пишет в память curl запрос
;;;;начни описывать имя так доступ к ...
(defun list-curl (curl-name cookie user-agent user-id access-token) "сфира пишет в память curl запрос"
       (list
	:curl-name curl-name
	:cookie cookie
	:user-agent user-agent
	:user-id user-id
	:access-token access-token
	))

;;;;(list-curl "доступ к каспи банку" "something-cookie" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/112.0")

;;;;Долли запоминает сфирот curla сначала во временную память temporary memorize memory curl
(defun t-memorize-MEMORY-CURL () "Долли запоминает сфирот curla сначала во временную память temporary memorize memory curl"
       (let ((curl-name (concat "Доступ к " (read-string "Название доступа - Доступ k... ")))
	     (cookie COOKIE)
	     (user-agent USER-AGENT)
	     (user-id USER-ID)
	     (access-token ACCESS-TOKEN))
	 ;;;;если переменные заданы
	 (when (not (string-empty-p cookie))
	   (and (not (string-empty-p user-agent)))
	   (and (not (string-empty-p user-id)))
	   (and (not (string-empty-p curl-name)))
	       ;;;;вставить список в переменную
	       (push (list-curl curl-name cookie user-agent user-id access-token) MEMORY-CURL)))
       (message "Привет еще раз. Я только что запомнила ключи для curl. Если напомнить то запомню надолго."))
;;;;(t-memorize-MEMORY-CURL)

;;;;Долли запоминает надолго. и после сна обязательно вспомнит!
(defun memorize-MEMORY-CURL (filename) "Долли запоминает надолго. и после сна обязательно вспомнит!"
       ;;;;;только нельзя запоминать атом пустоты - это уничтожит всю память!!!
       (when (not (eq MEMORY-CURL nil))
	 ;;;;создается временный буффер
	 (with-temp-buffer
	   (print MEMORY-CURL (current-buffer))
	   (write-file filename)))
       (message "Мне достаточно повторить пару раз - и я запомнила надолго! В MEMORY-CURL произвела запись...ok"))
;;;;;(memorize-MEMORY-CURL file-memory-url)

;;;;;вспоминание сразу всей памяти
(defun remember-MEMORY-CURL (file-memory-url) "вспоминание"
       (if (file-exists-p file-memory-url)
	   (with-temp-buffer
	     (insert-file-contents file-memory-url)
	     (setq MEMORY-CURL (read (current-buffer)))
	     (message "Вспомнила!"))
	 (message "Не могу найти такой файл!")))
;;;;;(remember-MEMORY-CURL file-memory-url)

;;;;;;;из всей памяти вспоминание нужных деталей
(defun remember-element-curl () "из всей памяти вспоминание нужных деталей"
       ;;;;;внутри сфиры счетчик - он должен быть сначала нулем!
       (when (not (eq i 0)) (setq i 0))
       ;;;;;детали которые нужно вспомнить
       (let ((curl-name (concat "Доступ к " (read-string "Доступ к чему нужен?... ")))
	     (memory-counter (length MEMORY-CURL)))
	 ;;;;все время пока счетчик меньше количества атомов в памяти
	 (while (< i memory-counter)
	   ;;;;если найден атом памяти по данному идентификтору-имени вернуть nil
	   (if (string= (getf (nth i MEMORY-CURL) :curl-name) curl-name)
	       ;;;;исследуем найденный атом - он состоит из конечных атомов - вот они и нужны!
	       (let ((user-agent (getf (nth i MEMORY-CURL) :user-agent))
		     (cookie (getf (nth i MEMORY-CURL) :cookie))
		     (user-id (getf (nth i MEMORY-CURL) :user-id))
		     (access-token (getf (nth i MEMORY-CURL) :access-token)))
		 ;;;;в этом месте будет сфира которая будет принимать эти исходные атомы и делать из них curl
		 (setq info-list (dolly-make-curl user-agent cookie user-id access-token))
		 ;;;;доводим счетчик до конца - тем самым завершая цикл
		 (setq i memory-counter))
           ;;;инкрементируем счетчик
	     (setq i (+ i 1))))))
;;;;(remember-element-curl)
