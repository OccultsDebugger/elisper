(defun bread-library-db ()
  "loads the list of books from disk file to the variable bread-libraray-db"
  (if (file-exists-p bread-library-file)
      (with-temp-buffer
	(insert-file-contents bread-library-file)
	(setq bread-libraray-db (read (current-buffer)))
	(setq bread-libraray-db '()))))
(bread-library-db)

(defun bread-library-add-book (file)
  "Attempts to get metadata from file, then promtps for 
confirmation (or modification) of these metada, then adds the book to the database and saves it.
Intended use: from dired."
  (if (assoc file bread-library-db)
      (error "File is already in the database")
    (progn
      (let ((metadata (bread-get-metadata file)))
	(let ((filename (nth 0 metadata))
	      (author (read-from-minibuffer
		       "Author: "
		       (nth 1 metadata)))
	      (title (read-from-minibuffer
		      "Title: "
		      (nth 2 metadata)))
	      (genre (read-from-minibuffer "Genre: " (nth 3 metadata)))
	      (tags (read-from-minibuffer "Tags (separated and surrounded by colons): " ":"))
	      (desc (nth 4 metadata)))
	  (setq bread-library-db (cons
				  (list filename author title tags "TOREAD" genre nil desc)
				  bread-library-db))))
      (bread-library-save-db bread-library-db))))

(defun bread-library-save-db (db)
  "Save the library database to a file."
  (message "Saving Bread library database...")
  (with-temp-buffer
    (insert "; 1.path 2.author 3.title 4.tags 5.state 6.genre 7.priority 8.description")
    (write-file bread-library-file))
  (message "Saving Bread library database...done"))
