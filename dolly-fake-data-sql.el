;;;;создать память, где Долли будет запоминать то что нужно вставить порядок
(defvar memory-fixture-key () "создать память, где Долли будет запоминать ключи, то есть имена таблиц")
(defvar memory-fixture-type () "создать память, где Долли будет запоминать значения, то есть тип таблиц")
(defvar memory-write-sql () "создать память, где Долли будет писать комманду для эскл")

(setq memory-fixture-key "memorize-fixture-key")
(setq memory-fixture-type "memorize-fixture-type")
(setq memory-fixture-write "memorize-fixture-write")

(defun dolly-fake-data-sql (host user password dbname table) "прога заполняет фейковыми данными mysql таблицу"
       (let* ((desc-key (format "mysql -h%s -u%s -p%s %s -Bse \"desc %s\" | awk '{ print $1} '" host user password dbname table))
	      (desc-type (format "mysql -h%s -u%s -p%s %s -Bse \"desc %s\" | awk '{ print $2} '" host user password dbname table))
	      (pre-memory-key (substring (shell-command-to-string desc-key) 0 -1))
	      (pre-memory-type (substring (shell-command-to-string desc-type) 0 -1))
	      (memorize-key (progn
			      (go memory-fixture-key)
			      (insert pre-memory-key)
			      (beginning-of-buffer)
			      (kill-line)
			      (delete-backward-char -1)
			      (save-buffer memory-fixture-key)
			      (switch-to-buffer (other-buffer))
			      (get-string-from-file memory-fixture-key)))
	      (memorize-type (progn
			       (go memory-fixture-type)
			       (insert pre-memory-type)
			       (beginning-of-buffer)
			       (kill-line)
			       (delete-backward-char -1)
			       (save-buffer memory-fixture-key)
			       (switch-to-buffer (other-buffer))
			       (get-string-from-file memory-fixture-key)))
	      (remember-key (with-temp-buffer
			      (insert-file-contents memory-fixture-key)
			      (split-string (buffer-string) "\n" t)))
	      (remember-type (with-temp-buffer
			       (insert-file-contents memory-fixture-type)
			       (split-string (buffer-string) "\n" t)))
	      (write-sql-command (progn
				   (go memory-fixture-write) 
				   (beginning-of-buffer)
				   (insert (format "insert into %s (" table))
				   (recursia-for-s remember-key)
				   (delete-backward-char 2)
				   (insert ")")
				   (insert " values (")
				   (recursia-for-type remember-type)
				   (delete-backward-char 2)
				   (insert ");")))
	      (forget... (shell-command-to-string "rm -rf memorize-fixture-write memorize-fixture-type memorize-fixture-key"))))
       (print '(выполнила --dolly-fake-data-sql--)))

;;;(dolly-fake-data-sql "31.31.203.139" "vakas-tools.ru" "hF7zX6jV1p" "vakas-tools.ru" "tools_base_amo_report_cond") 
;;;(dolly-fake-data-sql "localhost" "root" "password" "symfony" "tools_base_amo_report_cond") 

(defun recursia-for-s (s) "рекурсия вместо цикла"
       (cond ((null s) (print '(змея выжглась до пустого атома!)))
	     (t (insert (format "%s, " (car s))) (recursia-for-s (cdr s)))))
;;;(recursia-for-s xletter)

;;(setq infos(list "jacke" "queen" "king" "dolly"))
;;(setq infot(list "varchar(4040)" "text" "integer"))
;;(recursia-for-s infos)
;;(recursia-for-type infot)

(defun recursia-for-type (s) "рекурсия для типов с системой проверок"
       (cond ((null s) (print '(змея выжглась до пустого атома!)))
	     ((null (listp s)) (print '(сфира работает только со списком! должен быть список)))
	     ((with-temp-buffer
		(insert (car s))
		(beginning-of-line)
		(looking-at "int"))
	      (print '(найден интегер!))
	      (insert (format "%s, " (random 100)))
	      (recursia-for-type (cdr s)))
	     ((with-temp-buffer
		(insert (car s))
		(beginning-of-line)
		(looking-at "var"))
	      (print '(найдет varchar!))
	      (insert (format "\"Some-Name\", "))
	      (recursia-for-type (cdr s)))
	     ((with-temp-buffer
		(insert (car s))
		(beginning-of-line)
		(looking-at "text"))
	      (print '(найден text))
	      (insert (format "\"lorem ipsum text. Etiam vel neque nec dui dignissim bibendum.\", "))
	      (recursia-for-type (cdr s)))
	     ((with-temp-buffer
		(insert (car s))
		(beginning-of-line)
		(looking-at "datetime"))
	      (print '(найден datetime))
	      (insert "\"2020-02-02\", ")
	      (recursia-for-type (cdr s)))
	     ((with-temp-buffer
		(insert (car s))
		(beginning-of-line)
		(looking-at "tinyint"))
	      (print '(найден tinyint))
	      (insert (format "%s, " (random 10)))
	      (recursia-for-type (cdr s)))
	     (t (print '(змея расщепилась на атомы!)))))

;;;;(recursia-for-type type-letter)
;;;где то утечка памяти не могу понять где
;;;прога реально робит но буффер оперативки забивается. вот если емакс с нуля включить то все ок.
;;;решить и на будущее для Долли это очень важно - работа с памятью (линупсом)

(/= 1.1 1.0)
(not (eq 'jacke 'jacke))

(null (eq 'jacke 'queen))

(defun set-name ()
  (read-string "name? "))

(defun entiry-ask () "бесконечный вопрос"
       (cond ((equal (funcall (lambda () (read-string "name? "))) "jacke"))
	     (t (entiry-ask))))

(defun entire-ask () "бесконечный вопрос"
       (cond ((equal (set-name) "jacke") (print '(причина остановки змеи)))
	     (t (print '(сфира)) (entire-ask))))

;;;(entiry-ask)



;;(setq duck '(1 2 3 4 5))

;;(dolist (i duck)
;;  (insert (format "\n%i" i)))

(setq mst '(1 2 3))

(dolist (a mst)
  (dolist (p mst)
    (dolist (k mst)
      (if (and (/= a p) (/= p k) (/= p 2) (/= p 3) (/= k 3))
	  (insert (print (format "\n alesha=%d petya=%d kolya=%d" a p k)))))))


;;;alesha=3 petya=1 kolya=2
