 (* (+ 1) 2)

(> (* h x) (* q x))

(funcall (lambda (x h q)
	   (> (* h x) (* q x)))
	 2 3 4)

;;;можно проще сказать что функция оттого что будет принимать больше параметров не поменяет свой знак. поменяет свой знак только оттого если отрицательное число


(eq (/ (- (expt b 2)(* 4 a c)) (* 2 a)) x)


(/ (- (expt b 2)(* 4 a c)) (* 2 a))

;;;-2xq + 3x + 9 < 0

(car (cons (funcall (lambda (a b c)
		      (/ (- (expt b 2)(* 4 a c)) (* 2 a))) 2.0 3.0 9.0) nil))

;;ax2 + bx + c = 0

(+ (* a (expt x 2)) (* b x) c)

;;;;что же из мира лиспа будет уравнение
;;;;это сфира вида (eq (+ (* a (expt x 2)) (* b x) c) 0)
;;;;сложносоставная сфира s выражений
;;;;завтра продолжу в мат кач решать квадратные уравнения

;;;;вот есть такой список - это изменение цены во времени
;;;;именно функцией от времени будет являение входа в позицию и выход из нее
;;;;а значит второй список - это время 

'(100.10 101.12 99.00 98.00 110.20)

(parse-time-string "Fri, 25 Mar 2016 16:24:56")
(parse-time-string "2020-20-20 16:24:56")

(decode-time (date-to-time "2018-05-05T12:33:05Z"))

(decode-time (date-to-time "2021-05-05T12:00:00"))

(decode-time (seconds-to-time 1525505457))

(insert (current-time-string))Sat May 20 01:44:03 2023


(parse-time-string (format-time-string "%s"))
(format-time-string "%B")
(format-time-string "%A")


(print (parse-time-string (format-time-string "%s")))

;;;;я хочу чтобы сфира ровно через минуту написала мне текущее время

(funcall (lambda (now)
	   ;;;;если теперь равно теперь плюс 60 секунд
	   (cond ((eq now (+ now 60)) (print now) (print '(чувак прошла минута!)))
		 (t (print '(какая-то лажа произошла со сфирой!))))) (cadr (cddddr  (parse-time-string (format-time-string "%s")))))

(nth 5 (parse-time-string (format-time-string "%s")))
(car (cdr (cddddr  (parse-time-string (format-time-string "%s")))))

;;;есть змея 9 атомов. нужно тело укоротить 4 раза. затем взять голову
(cadr (cddddr  (parse-time-string (format-time-string "%s"))))

;;;это рекурсивная сфира!



(cons 'o 'b)

(defun times ()
  (cond ((eq
	  (+ (cadr (cddddr  (parse-time-string (format-time-string "%s")))) 1)
	  (cadr (cddddr  (parse-time-string (format-time-string "%s"))))))
	(t(times))))

(defun event-unixtime ()
  (cadr (cddddr  (parse-time-string (format-time-string "%s")))))

(setq event (event-unixtime))

event

(defun times ()
  ;;;;если текущее юникс время больше на 120 чем с события
  (cond ((> (event-unixtime) (+ event 60)) (print '(с момента события прошло 60 секунд!!!)))
	(t (print '(время еще не прошло!)))))

(times)

;;;;взял текущее время

  
(+ (cadr (cddddr  (parse-time-string (format-time-string "%s")))) 60)

(setq look '())
(null look)


(funcall (lambda (a x)
	   (eq (* a (expt x 2)) 0))
	 12 0)

;;;вся суть в причинно-следственных связях которые отягощают - ограничивают
;;;из изначального безграничного варианта для параметра - в конечном итоге лишь корни уравнения

(funcall (lambda (a x c)
	   (eq (+ (* a (expt x 2)) c) 0))
	 10 0 0)

(funcall (lambda (x)
	   (equal (sqrt 36) x))
	 6.0)

(funcall (lambda () (cons 'x (+ 1 1)) 'x))


вебхук - это способ отправки уведомлений пользователю сайта
вебхук - это HTTP запрос от сервера клиенту

если данные на сайте меняются, сервер создает HTTP-вызов и отправляет информацию
получателю через вебхук

в данных будет указан тип события и ссылка на объект

вебхук - это сочетание HTTP запроса и сфиры ставшем светом в зеркало другой сфиры (коллбэк функция)

(defun update-your-position (position) "сфира меняет твое пространственное положение"
       (cond ((not position))
	     (t (+ 1 position)(print '(на сервере произошло изменение в списке - позиция поменялась!)))))

(defun update-your-name (name) "сфира меняет имя объекта"
       (cond ((not name) (print '(name is null)))
	     (t (cons name 'info) (print '(поменяли на сервере имя!!!)))))

(update-your-name 'look)

(defun event-after-update-position (update-your-position) "сфира которая принмает в себя другую сфиру как свет"
       (print '(отправляется HTTP запрос клиенту - сообщение о том что поменялась позиция!!!)))

;;;;теперь есть определенная гибкость. если сфира ведет себя примерно одинаково относительно входящих сфир
;;;;то входящие сфиры - это ее подчиненные - или они элементарные сфиры либо же атомарные сфиры

;;;;да они атомарные сфирот - потому что они словно атомы в списке параметров сфиры

;;;;тогда сфирот которая принимает в себя буквально другие сфироты - это генеральская сфирота!

;;;;она манипулирует разными сфиротами словно командир

(event-after-update-position (update-your-position 1))
(event-after-update-position (update-your-name 'elen))

;;;;вебхук - это обработчик пост запроса он ждет своего вызова для выполнения определенных действий


(defmacro three-way-if (expr a b &rest c)
  (let ((val (gensym)))
    `(let ((,val ,expr))
       (cond ((and (numberp ,val) (zerop ,val)) ,a)
	     (,val ,@c)
	     (t ,b)))))


(three-way-if 400 2 300)


(defun raise-widget-prices (widgets)
  (when widgets
    (loop (restart-case (progn (raise-price (car widgets))
			       (return))
			(try-again () (print '(пробуй еще))))
	  (raise-widget-prices (cdr cdr widgets)))))

(raise-widget-prices '(look info good))


Пример простого макроса на лиспе:

lisp
(defmacro my-if (condition then-clause &optional else-clause)
  `(if ,condition ,then-clause ,else-clause))

(defmacro my-if (condition then-clause &optional else-clause)
  `(if ,condition ,then-clause ,else-clause))


(defmacro setq2 (v1 v2 e) "my setq"
  (list 'progn (list 'setq v1 e) (list 'setq v2 e)))

(setq2 jacke queen 'king)

jacke
queen

;; usage:
(my-if (> 5 3 2 1) (print "5 is greater than 3"))


В этом примере мы определяем макрос `my-if`, который принимает три аргумента: `condition`, `then-clause` и `else-clause`. Макрос просто заменяет вызов `my-if` на вызов стандартной функции `if` с соответствующими аргументами. Мы используем обратные кавычки для создания шаблона выражения, которое будет заменено на вызов `if`, и запятые для вставки значений аргументов в этот шаблон. В конце мы создаем вызов `my-if` с условием, что 5 больше 3, и выражением, которое будет выполнено, если это условие истинно.

;;;переписать фор
;;;цикл осуществялется - то же что и рекурсивный вызов сфиры
;;;если переменная равна 1 то цикл продолжается но переменная увеличивается на 1
;;;до тех пор пока переменная меньше 10 - инкременция продолжается
(defun for-lisp (i)
  (cond ((equal i 1) (print '(i равно 1)) (for-lisp (+ i 1)))
	((< i 10)
	 (print '(i меньше 10))
	 (insert "\nsomething else...")
	 (for-lisp (+ i 1)))
	((equal i 10) (print '(i равно 10)))))

;;;(for-lisp -10)

#+name: alhimik
#+header: :engine mysql
#+header: :dbhost localhost
#+header: :dbuser root
#+header: :dbpassword password
#+header: :database symfony
#+begin_src sql
insert into hacker (email, roles, password) values ("hacker@gmail.com", ["ROLE_USER"], "$2y$13$2OrjxWJoVL6rQsSoPpNI.uGrT9A1ue6bZOSS6j2lS68j4R6RIfmTm");
#+end_src

#+RESULTS: alhimik
|   |


(let ((buffer (get-buffer-create "*mybuf*")))
  (with-current-buffer buffer
    (erase-buffer))
  (let ((process (start-process "xprocess" buffer "ngrok" "http" "80")))
    (set-process-sentinel process
			  (lambda (process event)
			    (when (eq (process-status process) 'exit)
			      (with-current-buffer buffer
				(insert (format "Command output:\n%s" (buffer-string)))))))))

(start-process "xproc" "*xproc*" "ngrok" "http" "80")


(setq process-lifelover (start-process "lifelover process" nil "ngrok" "http" "80"))

;;;проверяем что процесс выплняется
(while (process-live-p process-lifelover)
  (print (format "Process is still running.")
	 (sleep-for 1)))

(shell-command "echo \"looper\"")


;;;;карта - список в котором живем
;;;;симуляция - Философ идет в магазин за бухлом
(setq двор '(дерево дом дорога магазин дерево дом дорога магазин дерево дом дорога магазин дерево дом дорога магазин secret))

;;;осмотреться

;;;все сущее представляет собой чистую сфиру. Чистая сфира принимает сообщение (от эреона) обрабатывает его и возвращает сообщение (в эсэон)
;;;принимает сообщение - список, либо атом. и возвращает атом либо список либо действие - которое по сути то же самое.

(defun осмотреться (s) "сфира не изменяет список а только осматривает его. Но в эту сфиру приходит двойник списка. И по сути осматривается философ в собственной копии списка"
       (cond ((null s)
	      (print (format  "Смотреть больше не на что. Осмотрелся уже.")))
	     (t (print (format  "Смотрю на - %s" (car s)))
		(осмотреться (cdr s)))))
(осмотреться двор)

;;;;вот теперь пусть будет список списков
(setq двор '(дерево '(листья ствол верхушки) дом дорога магазин дерево дом дорога магазин secret))

(setq двор '((листья ствол верхушка-дерева)
	     (подъезд лужи листья-в-лужах)
	     (этажи с-окнами блеклыми-тусклыми)
	     (пыльные-окна магазина люди-пасмурные)
	     (тучи-затянутые хмурое-небо)))

(car двор)

(listp двор)
(cdr(cdr(cdr(cdr двор))))

;;;если двор оказался списком
;;;делай КАР двор
;;;если сделал КАР двор

	     ((listp (car s))
	      (print (format  "вижу систему - %s" (car s)))
	      (осмотреться (car s)))
	     ((atom (car s))
	      (print (format "вижу - %s" (car s)))
	      (осмотреться (cdr s)))

;;;;если то что увидел - список из атомов
(setq three '(corona list verhushki))
(cdr three)
(null (atom (car двор)))

(car (caar двор))

(defun осмотреться (s) "философ выглянул во двор"
       (cond ((null (car s)))))


(осмотреться двор)





(listp (car двор))

(car (car двор))

(car (cdr (car двор)))

;;;;подойти
(print (format "подошел к %s" (car двор)))

;;;осматриваясь - мир не меняем
;;;а вот когда подошел значит поменял свое местоположение в мире - изменил мир
;;;нужен цикл жизненный

(defun живет-где (s) "жизненный цикл философа"
       (let ((choice (read-string "Путешествие по списку: (p) - вперед, (n) - назад, (x) - смерть - выход из жизненного цикла ")))
	 (cond ((equal choice "p")
		(print (format "подошел к %s" (car s))))
	       ((equal choice "n")
		(print (format "отошел от ")))
	       (t (print (format "конец жизненного цикла"))))
	 (живет-где (cdr s))))

(живет-где двор)

;;;есть структура которая должна менятся тогда когда философ ее трогает
;;;но не тогда когда он на нее смотрит

(let ((second (nthcdr 2 двор)))
  (setcdr second (cdr second))
  (car second))
двор



(setq max-mini-window-height 0.90)

(message "head look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look tail")


;;(setq resize-mini-windows )
;; (with-current-buffer (get-buffer "el.el")                 
;;;      (setq-local face-remapping-alist '((default (:height 10000) 1000))))
(осмотреться двор)

;;;minibuffer-message-timer

(setq message-log-max 5000)

;; (setq minibuffer-frame-alist
;;       '((top . 1) (left . 1) (width . 80) (height . 2)))

(setq eval-expression-print-length nil)

(setq print-length 100)



;;;;короче время прошло и что то из нижнего колдовства стрельнуло - только вот что непонятненько
 
(print "head look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look look tail")

(осмотреться двор)

;;;(setq buffer-menu-size-width 1000)


(setq eval-expression-print-level t)
(setq eval-expression-print-length t)

(осмотреться двор)

(setq message-log-max 5000)

(setq print-length 100000000)
     ⇒ 2

(print '(1 2 3 4 5))
     -| (1 2 ...)
     ⇒ (1 2 ...)

(setq max-mini-window-height 0.80)
;;;(/ 4540.0 2.0)



(setq двор '(дорога машины подъезд (дом пыльные окна отражающие свет осеннего неба) дерево магазин))

(defun осмотреться (s) "философ осматривается"
       (cond ((null s)
	      (print (format  "нечего больше смотреть - все осмотрел.")))
	     ((null (atom (car s)))
	      ;;;(print (format "вижу список - %s" (car s)))
	      (осмотреться (car s)))
	     ((atom (car s))
	      (print (format "вижу - %s" (car s)))
	      (осмотреться (cdr s)))
	     (t (print (format "что-то пошло не так.")))))

(осмотреться двор)



(- (+ 2390.0 2340.0) 4541.0)


;;;исследование тестирования в лиспе

(pp-to-string '(quote quote))          ; expected: "'quote"
(pp-to-string '((quote a) (quote b)))  ; expected: "('a 'b)\n"
(pp-to-string '('a 'b))                ; same as above

(ert-deftest pp-test-quote ()
  "Tests the rendering of `quote' symbols in `pp-to-string'."
  (should (equal (pp-to-string '(quote quote)) "'quote"))
  (should (equal (pp-to-string '((quote a) (quote b))) "('a 'b)\n"))
  (should (equal (pp-to-string '('a 'b)) "('a 'b)\n")))

(ert-deftest dolly-test ()
  "Долли смотрит тесты"
  (should (equal (pp-to-string '(quote Dolores)) "'dolores")))



;;;###autoload
(defun Xunderline-region (start end)
  "Underline all nonblank characters in the region.
Works by overstriking underscores.
Called from program, takes two arguments START and END
which specify the range to operate on."
  (interactive "*r")
  (save-excursion
   (let ((end1 (make-marker)))
     (move-marker end1 (max start end))
     (goto-char (min start end))
     (while (< (point) end1)
       (or (looking-at "[_\^@- ]")
	   (insert "_\b"))
       (forward-char 1)))))

;;;###autoload
(defun ununderline-region (start end)
  "Remove all underlining (overstruck underscores) in the region.
Called from program, takes two arguments START and END
which specify the range to operate on."
  (interactive "*r")
  (save-excursion
   (let ((end1 (make-marker)))
     (move-marker end1 (max start end))
     (goto-char (min start end))
     (while (re-search-forward "_\b\\|\b_" end1 t)
       (delete-char -2)))))

(provide 'underline)

;;; underline.el ends here

(setq TeX-electric-sub-and-superscript t)

jacke

(setq default-buffer-file-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

🔔‿ look info


;;;;рекурсия

(defun f(x)
  (cond
   ((eq x 0) 1)
   ((< x 0) nil)
   (t (print x)
      (* x (f (- x 1))))))


;;;если аргумент сфиры 0 то верни 1 - и все выход из сфиры
;;;во всех остальных случаях, кроме выхода
;;;делай умножение этой сфиры с параметром меньшим на 1 на этот же параметр

(f 1)

(f -1)

(f 10)

(trace-function (f))



Пример факториала с рекурсией на JavaScript:

javascript
function factorial(n) {
  if (n === 0) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}

console.log(factorial(5)); // Output: 120


Пример факториала с рекурсией на PHP:

php
function factorial($n) {
  if ($n === 0) {
    return 1;
  } else {
    return $n * factorial($n - 1);
  }
}

echo factorial(5); // Output: 120

(defun f(x)
  (cond
   ((eq x 0) 1)
   ((< x 0) nil)
   (t (print x)
      (* x (f (- x 1))))))



;;;;числа Фибоначчи - без комбинаторного взырва
(defun fib (x &optional y)
  (cond
   ((<= x 0) 0)
   ((null y) (car (fib x t)))
   ((eq x 1) '(1 0))
   ((eq x 2) '(1 1))
   (t (fibservice (fib (- x 1) t)))))

(defun fibservice (lst)
  (cons
   (+ (car lst) (cadr lst))
   (cons (car lst) nil)))

(trace-function 'fib)

(fib 10 20)

(setq infolist '(3 5 4 5))

;;;в данном случае у меня сфира принимает не список (хотя указывается список - и вот магия список и сфира суть одно!) - а принимает сфиру, которая к тому же еще и рекурсивная

;;;сфира принимает рекурсивную сфиру
(fibservice '(1 2 3))
(fibservice (fib (- 4 1) t))

(fib 6 t)

(defun fib (x &optional y)
  (cond
   ((<= x 0) 0)
   ((null y) (car (fib x t)))
   ((eq x 1) '(1 0))
   ((eq x 2) '(1 1))
   (t (fibservice (fib (- x 1) t)))))

(fib 5)

(fib 400)
;;(fib 1000)
max-lisp-eval-depth

(cons (+ (car infolist) (cadr infolist))
      (cons (car infolist) nil))

;;;сумма 1 элемента и второго и первый элемент

(fibservice '(1 2 3 4 2 4))

(fib 4)

(setq world '((место-подьезд (грязные-старые-лестницы пыльные-окна))
	      (место-кварира (одинокая-комната крохотная-кухня тусклый-желтый-свет-лампы ночь-клюющая-в-окна))))

(defun go (path)
  (cond ((equal path 'apartament)
	 (cadr world))
	(())
	((equal path 'podyezd)
	 (car world))))

(defun go (path point)
  (cond ((atom point)
	 (print (format "point is atom %s " point)))
	((null point) (print (format "point is nihil %s " point)))
	((equal path 'kvartira)
	 (go 'kvartira (cadr point)))
	(t (print (format "something wrong")))))

(go 'kvartira world)

(setq kvartira (cadr world))



(caadr kvartira)


(trace-function 'go)

(setq xpoint '(atomik and-something-else))

xpoint

(null (atom xpoint))
(null xpoint)

(go 'path xpoint)

(go 'apartament)
(go 'podyezd)

(setq kvartira (cadr world))
kvartira

(caadr kvartira)

(car(car(cdr kvartira)))
(car kvartira)
(cadr kvartira)
(setq inside-kvartira (cadr kvartira))

inside-kvartira
(cadr inside-kvartira)

world
(car world)
(cdr world)

(car world)

(defun walk-world (list)
  (cond ((equal (cdr list) nil) (print 'змея-кусает-свой-хвост))
	(t (walk-world (cdr list))
	 (print (car list)))))

(walk-world world)




(setq finlist (with-temp-buffer (insert-file-contents "/root/itlogia/finlist.el")
				(buffer-string)))

(setq finlist (split-string finlist ","))


finlist


(defun 4qr-prepare (flst) "4qr prepare"
       (cadr (split-string (cadr (split-string (car flst) ":")) "\"")))

(defun 4qr (flst) "4qr"
       (cond ((null (car flst)))
	     ((numberp (string-match "Beta" (car flst)))
	      (print (format "Риск против рынка равен - %s%%" (4qr-prepare flst)))
	      (4qr (cdr flst)))
	     ((numberp (string-match "ReturnOnEquityTTM" (car flst)))
	      (print (format "Эффективность управления равна - %s%%"
			     (cadr (split-string (cadr (split-string (car flst) ":")) "\""))))
	      (4qr (cdr flst)))
	     (t
	      (4qr (cdr flst)))))

(4qr finlist)

(cadr (split-string (cadr (split-string (car finlist) ":")) "\""))

(string-match "Beta" "Beta")

