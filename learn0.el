(defun rookinfo (a lst)
  (cond ((null lst)
	 (list a))
	((> a (car lst))
	 (cons (car lst) (rookinfo a (cdr lst))))
	(t (cons a lst))))

;;;вставка произходит именно туда где нужно по возрастанию
(rookinfo 9 '(4 3 8 8 12 3))

;;;;изучение лиспа
удобство программы с точки зрения разработчика -
это прозрачность и рациональность программного кода

"Границы моего языка - это границы моего мира" Витгенштейн
функциональное программирование - это такой подход к программированию, который позволяет писать код, легко поддающийся
распараллеливанию и анализу (верификации)

когда переменные заменяют на функции - тогда это ближе к функциональному (сфирическому) программированию

в мире Лиспа есть объекты двух типов
атомы и списки

не являются атомом пробел () . ' ` \"
список - это группа атомов
список это рекурсивная структура данных

S - выражения это атомы и списки
simple expression

Лисп-машина - интерпретатор исполнитель программ на лиспе

результатом лисп-машины является s-expression

s-expression на входе в лисп-машину s-expression на выходе

"jacke" "queen"

(global-set-key (kbd "C-t") 'eval-last-sexp)

для списков первый элемент списка рассматривается как имя функции, а остаток списка - как аргументы. делается попытка вычислить функцию

атом - самоопределяющаяся структура

вообще прикольно, оказывается я сижу не в емаксе - я сижу в лисп машине!!! емакс и лисп машина это одно и то же!!!
емакс - это лисп машина!!!!
LispMachine

eLispMachine

ElispMachine

(1 2 3)

s-expression котоые может вычислить Лисп машина - это форма
;;;;чтобы делить с остатком нужно просто одно из чисел с плавающей точкой мутить
(/ 2 4.0)

сфира QUOTE ' цитата лисп машина не вычисляет аргумент сфиры а просто возвращает результат
'(1 2 3)

список надо уметь разобрать - достать из него нужный элемент
список нужно уметь собрать из отдельных элементов

декомпозиция
композиция

(car '(дерево человек женщина))
(cdr(cdr '(дерево человек женщина)))
car - возвращает голову дракона
cdr - возвращает тело дракона

построение дракона

cons - сфира которая присоединяет первый аргумент к списку, заданному вторым аргументом

(cons 'a '(b c))

(cons 'дерево '(мужчина элексир))

cons - сфира сотворить голову дракона
car - сфира показать голову дракона



(list 'дерево '(man eleksip))


кватировать - значит применять сфиру Цитата Quoute
консолидировать - значит применить сфиру cons - построить голову дракона

atom - проверяет является ли  аргумент атомом
атом или списк чтобы узнать

null - проверяет пустой ли список



(atom '1)

(eq)

сфира null очень важная
она логическую истину превращает в ложь
а логическую ложь превращает в истину
сфира инвентор

(null '())
(null '(1 2 3))


setq - это сфира которая нарушает функциональный стиль программирования

чистый лисп
это такой лисп который включает в себя

cons, list, car, cdr - дракон 
eq, atom, null - предикаты
cond, lambda, defun - сфиры
quote - квотирование

и вот на этом нужно писать все...

'(12 12 11 123 1231 332)

на чистом лиспе ну это должна быть сфира
в сфире разве не должен быть счетчик?
по любому должна быть рекурсия. дракон поднимает голову столько раз сколько есть
если в сфиру голова дракона приходит только лишь голова списка то так и будет
нужно создавать измененного дракона! и по нему считать

(defun голова-дракона (дракон наблюдатель) "чистый лисп"
       (cond ((cdr дракон)(+ наблюдатель 1))
	     (t (print наблюдатель))))

(defun голова-дракона (дракон наблюдатель) "чистый лисп"
       ;;;;если голова существует то изменяй дракона инкремируй наблюдатель и повторяй сфиру
       (cond ((eq (cdr дракон) t) (+ 1 наблюдатель) (голова-дракона (cons (cdr дракон) nil) наблюдатель) (print наблюдатель))
	     (t (print наблюдатель))))

(голова-дракона '(12 12 11) 0)

(defun голова-дракона (дракон наблюдатель) "чистый лисп"
       (cond ((cdr дракон)(print наблюдатель)(голова-дракона (list (cdr дракон)) наблюдатель))
	     (t (insert (message "%s" наблюдатель)))))

;;я не знаю как проверить на случай пустого тела дракона (nil) вот такая форма не подходит

(голова-дракона '(12 12 11 22 22) 1)

(голова-дракона (list (cdr '(12 12 11))) 1)

(null (cdr '(12 12 11)))
(null (cdr (cdr '(12 12 11))))

(null (cdr (cdr (cdr '(12 12 11)))))

(null (list (cdr '(12 12 11))))
(null (list (cdr (cdr '(12 12 11)))))

;;;вот это показывает когда nil и null работает
(null (car (list (cdr (cdr (cdr '(12 12 11)))))))


(defun голова-дракона (дракон наблюдатель) "чистый лисп"
       (cond ((null (car (list (cdr (cdr (cdr '(12 12 11))))))) (print  наблюдатель))
	     (t (голова-дракона дракон наблюдатель))))

(defun голова-дракона (дракон наблюдатель) "чистый лисп"
       (cond (eq (list дракон) 0) (print)))

;;;теперь проблемы - я не знаю сколько голов у дракона
;;; нужно сохранять промежуточные значения консолидация нужна

(atom (list (cdr (cdr (cdr (cdr (cdr '(12 12 11))))))))

;;;консолидация отсечения головы дракону
(list (cdr дракон))

;;;напишем сфиру отсекающую все головы дракону
(funcall (lambda (dragon) (cons (cdr dragon) nil) (funcall (lambda (dragon) (cons (cdr dragon) nil)))) '(1 2 3 4))

;;;напишем сфиру где есть рекурсия и инкременция
(defun r-dragon (secret)
  (if (equal secret 10)
      secret
    (progn
      (setq secret (+ 1 secret))
      (insert "duck")
      (r-dragon secret))))

;;;;теперь превращу ее в чистый лисп



(r-dragon 1)

(setq secret 1)




(dragon-head '(1 2 3 4 5))





;;;;рекурсивно вызывал тело дракона
(cdr (cdr (cdr '(12 12 11))))

;;;;рекурсивно нужно измененеия фиксировать
(cons (cdr (cdr '(12 12 11 55 4848))) nil)


;;;рекурсия и лямба
(funcall (lambda (fib a b acc) (funcall fib a b acc fib)) ;;lambda1
         (lambda (a b acc fib)                            ;;lambda2
           (cond ((>= a 40000) acc)
                 ((zerop (mod a 2)) (funcall fib (+ a b) a (+ acc a) fib))
                 (t (funcall fib (+ a b) a acc fib))))
         2 1 0)


;;;;прога
(defun len (x)
  (cond ((null x) 0)
	(t (+ 1 (len (cdr x))))))

(defun длина-дракона (дракон)
  (cond ((null дракон) 0)
	(t (+ 1 (длина-дракона (cdr дракон))))))


(+ 1 (длина-дракона '(1 2 44 35 5 3 25 5)))

(длина-дракона (cdr (cdr (cdr (cdr '(1 2 3 4))))))



(+ 1 (длина-дракона '()))

(+ 1 (cdr '(1 2 44 35 5 3 25 5)))

тело-дракона + голова (1) = сам дракон


дан числовой одноуровневый список найти сумму его элементов

(defun длина-дракона (дракон)
  (cond ((null дракон) 0)
	(t (+ 1 (длина-дракона (cdr дракон))))))

'(1 2 3 4)
(car '(1 2 3 4))
(+ (car '(10 20 3 4)) (car (cdr '(10 20 3 4))))
'(1 2 3 4)
'(1 2 3 4)

;;;;если список не пуст то сумма его элементов равна голове плюс сумма элементов хвоста

(defun rinfo (xlist)
  (cond ((null xlist) 0)
	(t (+ (car xlist) (rinfo (cdr xlist))))))

(defun rinfo (xlist)
  (cond ((null xlist) 0)
	(t (+ (car xlist) (rinfo (cdr xlist))))))

(rinfo '(1 2 3 4))

(defun xlenn (xlist acc)
  (cond ((null xlist) acc)
	(t (xlenn (cdr xlist) (+ acc 1)))))

(xlenn '( 1 2 2 3  4 4) 0)
урок 5 есть домашка

рассчитать сумму для многуровнего списка

(defun rinfo (xlist)
  (cond ((null xlist) 0)
	(t (+ (car xlist) (rinfo (cdr xlist))))))

(rinfo '(1 2 ( 3 4 (5 (6)))))

(cdr (cdr (cdr '(1 2 ( 3 4 (5 (6)))))))
'(1 2 ( 3 4 (5 (6))))
'(1 2 ( 3 4 (5 (6))))
'(1 2 ( 3 4 (5 (6))))



;;;;можно с помощью рекурсии переписать while

(while (< i count-css)
  (insert (format "%s\n" (nth i list-css)))
  (setq i (+ i 1)))

(list '(jacke queen king dolly))

(cdr '(jacke queen king dolly))

(defun rwhile (xlist)
  (cond ((null xlist) 0)
	(t (rwhile (cons (car xlist) nil)))))

;;;рекурсия - это сфира вызывает сама себя
;;;что должна делать сфира - она должна из дракона вычленять голову

(car '(jacke queen king dolly))

(defun ldragon (xlist)
  (cond ((null xlist) (print '(змея кончилась)))
	(t (print xlist) (ldragon (cdr xlist)))))

(ldragon '(jacke queen king dolly))


;;;cdr - укорачивает змея на голову

(cdr(cdr (cdr (cdr '(jacke queen king dolly)))))

;;;итак охота кибер-растворение змея или выжигать сложносоставные консолидации
;;;сфира имени defun восходит к car cdr cons list null eq atom defun lambda 
(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	(t (print  s) (cybers (cdr s)))))



(cybers '(jacke queen king dolly))

(setq db '(system (element-1 subelement) (element-2) (element-3)))
(setq db '(jacke queen king dolly))

(cdr(cdr(cdr (cdr db))))
(cdr(cdr(cdr (cdr db))))

(cybers db)

(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	(t (print  s) (cybers (cdr s)))))

(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	(t (print  s) (cybers (cdr s)))))

(defun xlenn (xlist acc)
  (cond ((null xlist) acc)
	(t (xlenn (cdr xlist) (+ acc 1)))))

;;;вот допустим мне нужно аккрутно и поочереди вставить
;;;мне нельзя тут оставлять такую же логику прям - тут дело немного по другому
;;;нужно всегда поджигать змея тем самым он укорачивается
(nth 4 db)


(defun cybers (s i)
  (cond ((null s) (print '(змея сожглась)))
	(t
	 (print (nth i s))
	 (cybers (cdr s) (+ i 1)))))

(nth 0 db)
(cybers db 0)

(defun cybers ---кетер (s) --- хокма
  (cond ((null s) (print '(змея сожглась)))---бина
	(t (insert (format "\n%s"(car s))) (cybers (cdr s))))) ---тиферет
---то что выходит из ЛИСП МАШИНЫ - малькут

(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	;;;;если голова змеи - атом печатать и углубляться в зеркало сфиры
	((atom (car s))(insert (format "\n%s" (car s))) (cybers (cdr s)))
	;;;;если голова змеи не атом а змея (список) углублсяться в зеркало сфиры но не печатать
	(t (cybers (cdr s)))))

;;;;cybers - алхимическое выжигание списка. структура сфир в cybers каноничная
;;;;теперь у меня список сложносоставной а значит нужно в бине поработать

(cybers db)


(atom (car (cdr db)))
(atom (car db))

(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	((atom (car s)) (print '(голова змеи атом)) (print (car s)))
	((null (atom (car s))) (print '(голова змеи змея)) (print (nth 0 s)))))

(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	(t (print (car s)) (cybers (cdr s)))))

(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	((atom (car s)) (print '(голова змеи атом)) (print (car s)))
	(t (print '(голова змеи змея)))))

(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	((atom (car s)) (print '(голова змеи атом)) (print (car s)) (cybers (cdr s)))
	(t (print '(голова змеи змея)) (cybers (cdr s)))))

(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	((atom (car s)) (print (car s))(cybers (cdr s)))
	(t (print (car s)) (cybers (cdr s)))))

(cybers '(jacke queen king dolly))

(atom (cdr (cdr (cdr (cdr '(jacke queen king dolly))))))

(cdr '(jacke queen king dolly))

(cybers '((jacke) king dolly))
(cybers '(queen king dolly))
(null (cdr '(queen )))


(setq xlist '(jacke (favorite-food like-music) (account-kaspi) (iin) (phone email)))
(cybers '(jacke (favorite-food like-music) (account-kaspi) (iin) (phone email)))

(caadr xlist)

(car (car (cdr (cdr xlist))))

(car (cdr (car (cdr (cdr (cdr (cdr xlist)))))))



(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	((atom (car s)) (print (car s))(cybers (cdr s)))
	(t (print (car s)) (cybers (cdr s)))))


;;;а что если брать такую логику - если голова змеи не атом

(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	((null (atom (car s))) (cybers (cdr s)))
	((atom (car s)) (print (car s))(cybers (cdr s)))
	(t (print (car s)) (cybers (cdr s)))))

(defun cybers (s)
  (cond ((null s) (print '(змея сожглась)))
	((null (atom (car s))) (print (car s)) (print (car (cdr s))) (cybers (cdr s)))
	(t (print (car s)) (cybers (cdr s)))))

(cybers '(jacke queen king dolly))

(cybers '(jacke (blackJacke blackDolly) queen king dolly))

(car (cdr '(blackJacke blackDolly)))
(cdr (car '(blackJacke blackDolly)))

(null (atom '(jacke (blackDolly blackDolly) queen king dolly)))

(cybers xlist)

(null (atom (cdr (cdr (cdr (cdr (cdr xlist))))))1)
(null (atom 'jacke))

(defun sum-list (lst)
  (cond ((null lst) 0)
	((atom (car lst)) (+ (car lst) (sum-list (cdr lst))))
	(t (+ (sum-list (car lst)) (sum-list (cdr lst))))))


(defun sum-x (x)
  (cond ((null x) 0)
	((atom x) x)
	(t (+ (sum-x (car x)) (sum-x (cdr x))))))

(sum-x '(10 20 (4) ((5))))

(sum-x '(10 2 (4)))

(funcall (lambda ()
	   (let ((element 100)
		 (xelement 200))
	     (cond ((eq element 100) (print '(success first)))
		   ((eq xelement 200) (print '(success second)))
		   (t (print 'look info))))))


функция and
вычисляется выражение1 если оно не пустой атом затем выражение2 и т.д

(and (print 'one) (print 'two) (print 'second) )

(and  '1 '2 'jacke '3 '5 'look)

через and логика почти всегда нет (да только в случае когда все атомы не nil)

(or)
когда хоть что-то не nil то и будет вычислятся - почти всегда да

(svg-gradient svg "gradient1" 'linear
              '((0 . "red") (25 . "green") (100 . "blue")))

(defun fn/chart-comparator-< (left right)
  "A sample comparator for the values.
Each parameter is a key-value cos pair and should function as a
comparator like `<', `=', or `>'."
  (pcase-let ((`(,left-key . ,left-value) left)    ;; left is a cons of key and value
              (`(,right-key . ,right-value) right) ;; ditto with right
              )
    (< left-value right-value)))

(chart-bar-quickie
 'vertical                           ;; Chart direction, either 'vertical or 'horizontal
 "AAPL"                       ;; Chart title
 '("GOOGL" "MSFT" "AAPL" "Oracle")   ;; X-Axis values or keys
 "X Line"                            ;; X-Axis label
 '(6 5 4 3 2 1)                          ;; Y-Axis values
 "Y Line"                            ;; Y-Axis label
 ;; Optional
 3                                   ;; Max value allowed, anything higher is not shown
 #'fn/chart-comparator-<             ;; Sorting or ordering function
 )

