(with-temp-buffer
  (setq memory-fixture-key (read-file-name)))


(defun elisp-reborn (kill-ring) "elisp-reborn"
       (cond ((null kill-ring) (print '(кольцо смерти опустело!!!)))
	     (t (insert (format "'\n%s' - " (car kill-ring)))
		(elisp-reborn (cdr kill-ring)))))


(defun simplified-regexp-opt (strings &optional paren)
 (let ((parens
        (cond
         ((stringp paren)       (cons paren "\\)"))
         ((eq paren 'words)    '("\\<\\(" . "\\)\\>"))
         ((eq paren 'symbols) '("\\_<\\(" . "\\)\\_>"))
         ((null paren)          '("\\(?:" . "\\)"))
         (t                       '("\\(" . "\\)")))))
   (concat (car parens)
           (mapconcat 'regexp-quote strings "\\|")
           (cdr parens))))


(defun check-input (input)
  (cond
    ((stringp input)
      (if (string-match-p "^[A-Za-z]+$" input)
        "Input is valid"
        "Input contains non-alphabetic characters"))
    (t "Input must be a string")))

(check-input "1010jacke")


(defun check-grade (grade)
  (cond
    ((not (numberp grade))
      "Grade must be a number")
    ((< grade 0) (cond
                  ((> grade -10) "Grade is invalid")
                  (t "Grade is out of range")))
    ((> grade 100) (cond
                    ((< grade 110) "Grade is invalid")
                    (t "Grade is out of range")))
    (t "Grade is valid")))


(defun check-list (list)
  (cond
    ((not (listp list))
      "Input must be a list")
    ((not (every #'stringp list))
      "All elements in the list must be strings")
    ((not (every (lambda (x) (string-match-p "^[a-zA-Z]+$" x)) list))
      "All elements in the list must contain only letters")
    (t "List is valid")))


(every #'stringp '("look" "info"))

(every (lambda (s) (string-match-p "int" s)) type-letter)
(every (string-match "int" type-letter) type)

(progn
  (looking-at "int")
  (car type-letter))

(funcall '(looking-at "int") '(car type-letter))

(with-temp-buffer
  (insert (car type-letter))
  (beginning-of-line)
  (looking-at "int"))



(defun read-lines (filePath)
  "Return a list of lines of a file at filePath."
  (with-temp-buffer
    (insert-file-contents filePath)
    (split-string (buffer-string) "\n" t)))
