(defun customer (cust_id address city cust_type_cd fed_id postal_code state)
  (list
   :cust_id cust_id
   :address address
   :city city
   :cust_type_cd cust_type_cd
   :fed_id fed_id 
   :postal_code postal_code
   :state
   ))

(customer 1 "Kz" "Pavlodar" "1" "1" "0101" "Active")
